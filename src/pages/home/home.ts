import { Component,OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewPage } from '../new/new';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  constructor(public navCtrl: NavController) {

  }
  ngOnInit() {
    console.log('ngOnInit');
  }

  goToNewPage() {
    this.navCtrl.push(NewPage, {name: 'andre', umur: 25});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HOME');
  }
  ionViewWillEnter() {
    console.log('ionViewWillEnter HOME');
  }
}