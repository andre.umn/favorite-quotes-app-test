import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new',
  templateUrl: 'new.html',
})
export class NewPage implements OnInit{
  private umur:number;
  private nama:string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  //angular lifecycle hooks, called when angular has already done setting the components
  ngOnInit() {
    this.nama = this.navParams.get('name');
    this.umur = this.navParams.get('umur');
    console.log('ngOnInit');
  }

  //ionic lifecycle hooks, called when the navController's page is loaded'
  ionViewDidLoad() {
    console.log('ionViewDidLoad NewPage');
  }
  ionViewWillEnter() {
    console.log('ionViewWillEnter NewPage');
  }
}
